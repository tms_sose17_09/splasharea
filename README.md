# Splash Area

## Synopsis
Die in diesem Repository hinterlegte Applikation ist ein Beleg, der für den Studiengang Angewandte Informatik im Fach Technik Mobiler Systeme an der Hochschule für Technik und Wirtschaft angefertigt wurde.
Mit Hilfe der Applikation ist man in der Lage, sich die Schwimm- bzw. Badestellen im Raum Berlin-Brandenburg anzeigen zu lassen.
---
## Motivation
"BADESTELLEN SUCHEN, FINDEN UND ABKÜHLEN."

Diese Applikation stellt eine weitere Möglichkeit dar, die sonnigen Momente des Lebens in "cooler" Umgebung zu genießen. Sie zeigt die unterschiedlichsten Bademöglichkeiten im Raum Berlin-Brandenburg an.

![StartActivity](concept/graphics/readmeGraphics/StartActivity3.png)![ListFragmentView](/concept/graphics/readmeGraphics/ListFragmentWithAddress.png)![MMapViewFragmentWithGps](/concept/graphics/readmeGraphics/MapViewFragmentWithGps.png)![IInformationActivityBrandenburg01](concept/graphics/readmeGraphics/InformationActivityBrandenburg01.png)

---
## Getting Started
#### Voraussetung
Das OS muss die Google Api's unterstützen (Google Services, Play Store), damit SplashArea verwendet werden kann.

#### Beschaffung
Die Applikation lässt sich über die folgenden Wege beziehen.
##### Manuelle Installation mittels Apk
Die signierte Apk befindet sich unter `application/app/app-release.apk`, die Sie unter Verwendung Ihres Smartphones, herunterladen und anschließend installieren können.
##### Git-Repo
Alternativ lässt sich das komplette Repository clonen

    git clone https://NUTZERNAME@bitbucket.org/tms_sose17_09/splasharea.git

und via Androidstudio öffnen. Hierzu starten Sie Androidstudio und gelangen über den Menüeintrag File -> Open... in die Verzeichnisübersicht. Anschließend navigieren Sie in das application-Verzeichnis des geclonten Repository und bestätigen die Auswahl.

---
## Project Structure
![StartActivity](/concept/graphics/readmeGraphics/ProjectStructureEdit.png)

---
## Permissions
* Standort [ACCESS FINE LOCATION]
* Standort [READ GSERVICES]
* Standort [ACCESS COARSE LOCATION]
* Internet [INTERNET]
* Speicherzugriff [WRITE EXTERNAL STORAGE]

---
## Open Data
Badestellen Berlin

Übersicht
[https://daten.berlin.de/datensaetze/liste-der-badestellen](https://daten.berlin.de/datensaetze/liste-der-badestellen)    

gjson-Datei
[http://www.berlin.de/lageso/gesundheit/gesundheitsschutz/badegewaesser/liste-der-badestellen/index.php/index/all.gjson?q=](http://www.berlin.de/lageso/gesundheit/gesundheitsschutz/badegewaesser/liste-der-badestellen/index.php/index/all.gjson?q=)

Badestellen in Brandenburg

Übersicht
[https://badestellen.brandenburg.de/home/-/bereich/karte](https://badestellen.brandenburg.de/home/-/bereich/karte)

kml-Datei
[https://badestellen.brandenburg.de/luis-baden/export/badestellen.kml?portalPage=/home](https://badestellen.brandenburg.de/luis-baden/export/badestellen.kml?portalPage=/home)

---
## Members
* Alexander Lüdke
* Josephine Ernst
* Nils Brandt
* Benjamin Molnar
---
## Contributors
Falls sie konstruktive Anregungen haben, um die Applikation weiter zu entwickeln, können Sie uns jeder Zeit via Email anschreiben oder einfach einen eigenen Fork erstellen.

---
