package de.htw_berlin.tms_sose17_09.splasharea;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * @version 0.1
 * @author Team SplashArea
 */

@RunWith(AndroidJUnit4.class)
public class FilterActivityEspressoTest {
    @Rule
    public ActivityTestRule<FilterActivity> mActivityRule =
            new ActivityTestRule<>(FilterActivity.class);

    @Test
    public void TestFilterActivity() {
        onView(withId(R.id.checkBox_berlin)).check(matches(isDisplayed()));
        onView(withId(R.id.checkBox_brandenburg)).check(matches(isDisplayed()));
        onView(withId(R.id.checkBox_parkingplace)).check(matches(isDisplayed()));
        onView(withId(R.id.checkBox_lifeguard)).check(matches(isDisplayed()));
        onView(withId(R.id.checkBox_gastronomy)).check(matches(isDisplayed()));
        onView(withId(R.id.checkBox_aquatic)).check(matches(isDisplayed()));
        onView(withId(R.id.checkBox_grillarea)).check(matches(isDisplayed()));
        onView(withId(R.id.checkBox_toilette)).check(matches(isDisplayed()));
        onView(withId(R.id.checkBox_playground)).check(matches(isDisplayed()));
        onView(withId(R.id.button_filtered)).check(matches(isDisplayed()));
        onView(withId(R.id.button_reset)).check(matches(isDisplayed()));
    }
}
