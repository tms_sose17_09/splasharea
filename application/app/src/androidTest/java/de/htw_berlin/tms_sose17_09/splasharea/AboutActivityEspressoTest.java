package de.htw_berlin.tms_sose17_09.splasharea;

/**
 * Created by ernstj on 14.07.17.
 */

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * @version 0.1
 * @author Team SplashArea
 */

@RunWith(AndroidJUnit4.class)
public class AboutActivityEspressoTest {
    @Rule
    public ActivityTestRule<AboutActivity> mActivityRule =
            new ActivityTestRule<>(AboutActivity.class);

    @Test
    public void TestAboutBrowseActivity() {
        onView(withId(R.id.textView_about_app_title)).check(matches(isDisplayed()));
        onView(withId(R.id.textView_developer)).check(matches(isDisplayed()));
        onView(withId(R.id.textView_developer_01)).check(matches(isDisplayed()));
        onView(withId(R.id.textView_developer_02)).check(matches(isDisplayed()));
        onView(withId(R.id.textView_developer_03)).check(matches(isDisplayed()));
        onView(withId(R.id.textView_developer_04)).check(matches(isDisplayed()));
        onView(withId(R.id.textView_git_repo_title)).check(matches(isDisplayed()));
        onView(withId(R.id.textView_git_repo_url)).check(matches(isDisplayed()));
        onView(withId(R.id.textView_data_sources)).check(matches(isDisplayed()));
        onView(withId(R.id.textView_data_brandenburg_overview)).check(matches(isDisplayed()));
        onView(withId(R.id.textView_data_berlin_overview)).check(matches(isDisplayed()));
        onView(withId(R.id.textView_version_title)).check(matches(isDisplayed()));
        onView(withId(R.id.textView_version_nr)).check(matches(isDisplayed()));
        onView(withId(R.id.textView_last_update)).check(matches(isDisplayed()));
        onView(withId(R.id.textView_last_update_date)).check(matches(isDisplayed()));
    }

}
