package de.htw_berlin.tms_sose17_09.splasharea;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * @version 0.1
 * @author Team SplashArea
 *
 * Dieser UI Test, testet, ob der Startbutton auf die naechste Activity leitet
 */
@RunWith(AndroidJUnit4.class)
public class StartActivityEspressoTest {

    @Rule
    public ActivityTestRule<StartActivity> mActivityRule =
            new ActivityTestRule<>(StartActivity.class);

    @Test
    public void clickButton_newActivity() throws Exception {
        // press the first button.
        onView(withId(R.id.next_button)).perform(click());

        // This view is in a different Activity, no need to tell Espresso.
        onView(withId(R.id.textView_parsing)).check(matches(withText("... parsing ...")));
    }

//    @Rule
//    public ActivityTestRule<MainActivity> mMainActivityRule =
//            new ActivityTestRule<>(MainActivity.class);
//
//    @Test
//    public void clickButton_MainActivity() throws Exception {
//        // press the first button.
//        onView(withId(R.id.list_view_SplashArea)).check(matches(isDisplayed()));
//        Log.i("", "oassdfadfasdf");


        // This view is in a different Activity, no need to tell Espresso.
       // onView(withId(R.id.textView_parsing)).check(matches(withText("... parsing ...")));
   // }

}
