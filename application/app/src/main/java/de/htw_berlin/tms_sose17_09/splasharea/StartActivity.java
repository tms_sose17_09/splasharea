package de.htw_berlin.tms_sose17_09.splasharea;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.htw_berlin.tms_sose17_09.splasharea.asyncTasks.CheckInternetConnectionTask;
import de.htw_berlin.tms_sose17_09.splasharea.models.SplashAreaSingleton;

/**
 * @version 0.17.0507
 * @author Team SplashArea
 *
 * Die Klasse stellt die Startansicht dar, welche nur einmal aufgerufen wird und andschliessend nach dem
 * Betaetigen des Buttons beendet wird (finish()).
 * Der Intent stellt nur die Verbindung zur MainActivity dar.
 */
public class StartActivity extends AppCompatActivity {
    private final String TAG = this.getClass().getSimpleName();

    private static boolean isStarting = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");

        // Goto MainActivity if the app has started
        if (isStarting) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        //new CheckInternetConnectionTask(getApplicationContext()).execute();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_start_activity);

        Typeface typefaceTexRegular = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font_tex_regular));
        Typeface typefaceTexBold = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font_tex_bolt));

        TextView textViewShortText = (TextView) findViewById(R.id.textView_short_text);
        textViewShortText.setTypeface(typefaceTexRegular);

        TextView textViewLongText = (TextView) findViewById(R.id.textView_long_text);
        textViewLongText.setTypeface(typefaceTexRegular);

        Button buttonNext = (Button) findViewById(R.id.next_button);
        buttonNext.setTypeface(typefaceTexBold);
    }

    public void onClickGoToMap(View Button) throws InterruptedException {
        Log.d(TAG, "onClickGoToMap");

        new CheckInternetConnectionTask(getApplicationContext()).execute();

        for(int i = 0; i <= 10000000; i++){}

        isStarting = true;

        // get alertDialog when there is no internetconnection
        if(!SplashAreaSingleton.getInstance().isInternetAvailable()) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(getResources().getString(R.string.no_internet_dialog_title));
            alertDialogBuilder.setMessage(getResources().getString(R.string.no_internet_dialog));
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.button_next_from_dialog), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    final Intent intent = new Intent(getApplicationContext(), MainActivity.class);

                    startActivity(intent);
                    finish();
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        } else {
            final Intent intent = new Intent(this, MainActivity.class);

            startActivity(intent);
            finish();
        }
    }

    @Override
    public void finish() {
        super.finish();
        Log.i(TAG, "finish");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
    }
}
