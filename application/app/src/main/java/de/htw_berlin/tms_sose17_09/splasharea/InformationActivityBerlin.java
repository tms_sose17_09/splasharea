package de.htw_berlin.tms_sose17_09.splasharea;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import de.htw_berlin.tms_sose17_09.splasharea.models.SplashAreaSingleton;

/**
 * @version 0.17.0507
 * @author Team SplashArea
 *
 * Die Klasse haelt die Informationen, welche fuer den Informationsscreen benötigt werde.
 */
public class InformationActivityBerlin extends AppCompatActivity {

    private final String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");

        setContentView(R.layout.layout_information_berlin_activity);

        // get intent extra
        // the id-value from the item which is sending from the MainActivity-ListFragment->Entry
        final Bundle extras = getIntent().getExtras();

        // get singleton for unsing the list
        SplashAreaSingleton splashAreaSingleton = SplashAreaSingleton.getInstance();

        // searching id into the SingletonList
        int id = Integer.parseInt(extras.getString("id"));

        TextView textViewBadname = (TextView) findViewById(R.id.textView_info_badname);
        TextView textViewAdresse = (TextView) findViewById(R.id.textView_info_address);
        TextView textViewBezirk = (TextView) findViewById(R.id.textView_info_bezirk);
        TextView textViewProfil = (TextView) findViewById(R.id.textView_info_profil);
        TextView textViewKoordinaten = (TextView) findViewById(R.id.textView_info_coordinate);
        TextView textViewTemperatur = (TextView) findViewById(R.id.textView_info_temp);
        TextView textViewWasserqualitaet = (TextView) findViewById(R.id.textView_info_waterquality);
        TextView textViewStand = (TextView) findViewById(R.id.textView_info_stand);

        textViewBadname.setText(splashAreaSingleton.getSplashAreaList().get(id).getBadname());
        textViewAdresse.setText(splashAreaSingleton.getSplashAreaList().get(id).getAdresse());
        textViewBezirk.setText(splashAreaSingleton.getSplashAreaList().get(id).getBezirk());
        textViewProfil.setText(splashAreaSingleton.getSplashAreaList().get(id).getProfil());
        textViewKoordinaten.setText(GeoFormatter.geoDataformatting(splashAreaSingleton.getSplashAreaList().get(id).getLongitude(), splashAreaSingleton.getFilteredSplashAreaList().get(id).getLatitude()));
        textViewTemperatur.setText(splashAreaSingleton.getSplashAreaList().get(id).getTemp() + "° C");
        textViewWasserqualitaet.setText(String.valueOf(splashAreaSingleton.getSplashAreaList().get(id).getWasserqualitaet()));
        textViewStand.setText(splashAreaSingleton.getSplashAreaList().get(id).getStand());
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
    }

    @Override
    public void finish() {
        super.finish();
        Log.i(TAG, "finish");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");
    }
}
