package de.htw_berlin.tms_sose17_09.splasharea;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.htw_berlin.tms_sose17_09.splasharea.filters.CriteriaBarbecueArea;
import de.htw_berlin.tms_sose17_09.splasharea.filters.CriteriaBerlin;
import de.htw_berlin.tms_sose17_09.splasharea.filters.CriteriaBrandenburg;
import de.htw_berlin.tms_sose17_09.splasharea.filters.CriteriaGastronomy;
import de.htw_berlin.tms_sose17_09.splasharea.filters.CriteriaLavatory;
import de.htw_berlin.tms_sose17_09.splasharea.filters.CriteriaLifeguard;
import de.htw_berlin.tms_sose17_09.splasharea.filters.CriteriaParkingArea;
import de.htw_berlin.tms_sose17_09.splasharea.filters.CriteriaPlayground;
import de.htw_berlin.tms_sose17_09.splasharea.filters.CriteriaAquatic;
import de.htw_berlin.tms_sose17_09.splasharea.filters.FilterCriteria;
import de.htw_berlin.tms_sose17_09.splasharea.filters.SpecificCriteria;
import de.htw_berlin.tms_sose17_09.splasharea.models.SplashAreaSingleton;

/**
 * @version 0.17.0507
 * @author Team SplashArea
 *
 * Diese Klasse stellt die Filterauswahl dar.
 */
public class FilterActivity extends Activity {

    private final String TAG = this.getClass().getSimpleName();

    private Context applicationContext;
    private CheckBox checkBoxBerlin;
    private CheckBox checkBoxBrandenburg;
    private CheckBox checkBoxParkingArea;
    private CheckBox checkBoxLifeguard;
    private CheckBox checkBoxGastronomy;
    private CheckBox checkBoxBarbecueArea;
    private CheckBox checkBoxLavatory;
    private CheckBox checkBoxPlayground;
    private CheckBox checkBoxAquatic;
    private Button buttonFiltered;
    private Button buttonReset;

    private final List<CheckBox> checkBoxList = new ArrayList<>();

    private final List<FilterCriteria> filterCriteriaList = new ArrayList<>();

    private static HashMap<String, Boolean> filterStateMap = new HashMap();

    private boolean showAllAreas[] = new boolean[2];

    private FilterCriteria filterCriteriaBerlin;
    private FilterCriteria filterCriteriaBarbecueArea;
    private FilterCriteria filterCriteriaBrandenburg;
    private FilterCriteria filterCriteriaGastronomy;
    private FilterCriteria filterCriteriaLavatory;
    private FilterCriteria filterCriteriaLifeguard;
    private FilterCriteria filterCriteriaParkingArea;
    private FilterCriteria filterCriteriaPlayground;
    private FilterCriteria filterCriteriaAquatic;
    //SpecificCriteria specificCriteria;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");

        setContentView(R.layout.layout_filter_activity);

        initialUISetup();
    }

    private void initialUISetup() {

        Typeface typefaceTexRegular = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font_tex_regular));

        checkBoxBerlin = (CheckBox) findViewById(R.id.checkBox_berlin);
        checkBoxBerlin.setTypeface(typefaceTexRegular);

        checkBoxBrandenburg = (CheckBox) findViewById(R.id.checkBox_brandenburg);
        checkBoxBrandenburg.setTypeface(typefaceTexRegular);

        checkBoxParkingArea = (CheckBox) findViewById(R.id.checkBox_parkingplace);
        checkBoxParkingArea.setTypeface(typefaceTexRegular);

        checkBoxLifeguard = (CheckBox) findViewById(R.id.checkBox_lifeguard);
        checkBoxLifeguard.setTypeface(typefaceTexRegular);

        checkBoxGastronomy = (CheckBox) findViewById(R.id.checkBox_gastronomy);
        checkBoxGastronomy.setTypeface(typefaceTexRegular);

        checkBoxAquatic = (CheckBox) findViewById(R.id.checkBox_aquatic);
        checkBoxAquatic.setTypeface(typefaceTexRegular);

        checkBoxBarbecueArea = (CheckBox) findViewById(R.id.checkBox_grillarea);
        checkBoxBarbecueArea.setTypeface(typefaceTexRegular);

        checkBoxLavatory = (CheckBox) findViewById(R.id.checkBox_toilette);
        checkBoxLavatory.setTypeface(typefaceTexRegular);

        checkBoxPlayground = (CheckBox) findViewById(R.id.checkBox_playground);
        checkBoxPlayground.setTypeface(typefaceTexRegular);

        buttonFiltered = (Button) findViewById(R.id.button_filtered);
        buttonFiltered.setTypeface(typefaceTexRegular);

        buttonReset = (Button) findViewById(R.id.button_reset);
        buttonReset.setTypeface(typefaceTexRegular);

        // add to CheckedListener
        checkBoxBerlin.setOnCheckedChangeListener(new checkBoxClickListener());
        checkBoxBrandenburg.setOnCheckedChangeListener(new checkBoxClickListener());
        checkBoxParkingArea.setOnCheckedChangeListener(new checkBoxClickListener());
        checkBoxLifeguard.setOnCheckedChangeListener(new checkBoxClickListener());
        checkBoxGastronomy.setOnCheckedChangeListener(new checkBoxClickListener());
        checkBoxAquatic.setOnCheckedChangeListener(new checkBoxClickListener());
        checkBoxBarbecueArea.setOnCheckedChangeListener(new checkBoxClickListener());
        checkBoxLavatory.setOnCheckedChangeListener(new checkBoxClickListener());
        checkBoxPlayground.setOnCheckedChangeListener(new checkBoxClickListener());

        // Initial FilterLists
        filterCriteriaBerlin = new CriteriaBerlin();
        filterCriteriaBarbecueArea = new CriteriaBarbecueArea();
        filterCriteriaBrandenburg = new CriteriaBrandenburg();
        filterCriteriaGastronomy = new CriteriaGastronomy();
        filterCriteriaLavatory = new CriteriaLavatory();
        filterCriteriaLifeguard = new CriteriaLifeguard();
        filterCriteriaParkingArea = new CriteriaParkingArea();
        filterCriteriaPlayground = new CriteriaPlayground();
        filterCriteriaAquatic = new CriteriaAquatic();

        // "CheckButtonGroup" will be used for "Berlin" because they don't have these attributes.
        checkBoxList.add(checkBoxParkingArea);
        checkBoxList.add(checkBoxLifeguard);
        checkBoxList.add(checkBoxGastronomy);
        checkBoxList.add(checkBoxAquatic);
        checkBoxList.add(checkBoxBarbecueArea);
        checkBoxList.add(checkBoxLavatory);
        checkBoxList.add(checkBoxPlayground);

    }

    private class checkBoxClickListener implements CheckBox.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            Log.i(TAG, "onCheckedChanged");

            Iterator<CheckBox> iteratorCheckBox = checkBoxList.iterator();

            // box is selected
            if(isChecked) {

                if(buttonView == checkBoxBerlin) {
                    while (iteratorCheckBox.hasNext()) {
                        CheckBox tempCheckbox = iteratorCheckBox.next();
                        // disable checkboxes. Options which are not useable for SplashAreas in Berlin.
                        tempCheckbox.setEnabled(false);
                        tempCheckbox.setChecked(false);
                    }
                    showAllAreas[0] = true;
                    filterStateMap.put("checkBoxBerlin", true);
                    filterCriteriaList.add(filterCriteriaBerlin);
                }

                if(buttonView == checkBoxBrandenburg) {
                    showAllAreas[1] = true;
                    filterStateMap.put("checkBoxBrandenburg", true);
                    filterCriteriaList.add(filterCriteriaBrandenburg);
                }

                if(buttonView == checkBoxBarbecueArea) {
                    filterStateMap.put("checkBoxBarbecueArea", true);
                    filterCriteriaList.add(filterCriteriaBarbecueArea);
                }

                if(buttonView == checkBoxGastronomy) {
                    filterStateMap.put("checkBoxGastronomy", true);
                    filterCriteriaList.add(filterCriteriaGastronomy);
                }

                if(buttonView == checkBoxAquatic) {
                    filterStateMap.put("checkBoxAquatic", true);
                    filterCriteriaList.add(filterCriteriaAquatic);
                }

                if(buttonView == checkBoxLavatory) {
                    filterStateMap.put("checkBoxLavatory", true);
                    filterCriteriaList.add(filterCriteriaLavatory);
                }

                if(buttonView == checkBoxLifeguard) {
                    filterStateMap.put("checkBoxLifeguard", true);
                    filterCriteriaList.add(filterCriteriaLifeguard);
                }

                if(buttonView == checkBoxParkingArea) {
                    filterStateMap.put("checkBoxParkingArea", true);
                    filterCriteriaList.add(filterCriteriaParkingArea);
                }

                if(buttonView == checkBoxPlayground) {
                    filterStateMap.put("checkBoxPlayground", true);
                    filterCriteriaList.add(filterCriteriaPlayground);
                }

            }

            // box is unselected
            if(!isChecked) {

                if(buttonView == checkBoxBerlin) {
                    while (iteratorCheckBox.hasNext()) {
                        CheckBox tempCheckbox = iteratorCheckBox.next();
                        tempCheckbox.setEnabled(true);
                    }
                    showAllAreas[0] = false;
                    filterStateMap.remove("checkBoxBerlin");
                    // update the criteria list we want to filter.
                    filterCriteriaList.remove(findPositionInArrayList(filterCriteriaList, new CriteriaBerlin()));
                }

                if(buttonView == checkBoxBrandenburg) {
                    showAllAreas[1] = false;
                    filterStateMap.remove("checkBoxBrandenburg");
                    filterCriteriaList.remove(findPositionInArrayList(filterCriteriaList, new CriteriaBrandenburg()));
                }

                if(buttonView == checkBoxBarbecueArea) {
                    filterStateMap.remove("checkBoxBarbecueArea");
                    filterCriteriaList.remove(findPositionInArrayList(filterCriteriaList, new CriteriaBarbecueArea()));
                }

                if(buttonView == checkBoxGastronomy) {
                    filterStateMap.remove("checkBoxGastronomy");
                    filterCriteriaList.remove(findPositionInArrayList(filterCriteriaList, new CriteriaGastronomy()));
                }

                if(buttonView == checkBoxAquatic) {
                    filterStateMap.remove("checkBoxAquatic");
                    filterCriteriaList.remove(findPositionInArrayList(filterCriteriaList, new CriteriaAquatic()));
                }

                if(buttonView == checkBoxLavatory) {
                    filterStateMap.remove("checkBoxLavatory");
                    filterCriteriaList.remove(findPositionInArrayList(filterCriteriaList, new CriteriaLavatory()));
                }

                if(buttonView == checkBoxLifeguard) {
                    filterStateMap.remove("checkBoxLifeguard");
                    filterCriteriaList.remove(findPositionInArrayList(filterCriteriaList, new CriteriaLifeguard()));
                }

                if(buttonView == checkBoxParkingArea) {
                    filterStateMap.remove("checkBoxParkingArea");
                    filterCriteriaList.remove(findPositionInArrayList(filterCriteriaList, new CriteriaParkingArea()));
                }

                if(buttonView == checkBoxPlayground) {
                    filterStateMap.remove("checkBoxplayground");
                    filterCriteriaList.remove(findPositionInArrayList(filterCriteriaList, new CriteriaPlayground()));
                }
            }
        }
    }

    /**
     * to get the index of the latest filtercriteria classes (List).
     * @param searchList
     * @param search
     * @return
     */
    private int findPositionInArrayList(List<FilterCriteria> searchList, FilterCriteria search) {
        Log.i(TAG, "findPositionInArrayList");

        for (FilterCriteria filterCriteria : searchList) {
            if (filterCriteria.getClass().getName().equals(search.getClass().getName())) {
                return searchList.indexOf(filterCriteria);
            }
        }
        return -1;
    }

    /**
     * Use the filtercriteria to create the filteredlist into the singleton (SplashAreaSingleton)
     */
    public void applyFilter(View Button) {
        Log.i(TAG, "applyFilter");

        if(showAllAreas[0] == true && showAllAreas[1] == true) {
            SplashAreaSingleton.getInstance().setFilteredSplashAreaList(SplashAreaSingleton.getInstance().getSplashAreaList());
        } else {
            // create the filteredList >>> it's magic ... bam bam bam <<<
            SplashAreaSingleton.getInstance().setFilteredSplashAreaList(new SpecificCriteria(filterCriteriaList).collectCriteria(SplashAreaSingleton.getInstance().getSplashAreaList()));
        }

        finish();
    }

    public void resetFilter(View Button) {
        Log.i(TAG, "resetFilter");

        // reset to display berlin splashareas
        filterCriteriaList.clear();
        filterCriteriaList.add(filterCriteriaBerlin);

        // create the filteredList >>> it's magic ... bam bam bam <<<
        SplashAreaSingleton.getInstance().setFilteredSplashAreaList(new SpecificCriteria(filterCriteriaList).collectCriteria(SplashAreaSingleton.getInstance().getSplashAreaList()));

        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");


        Set set = filterStateMap.entrySet();
        Iterator iterator = set.iterator();

        while(iterator.hasNext()) {
            Map.Entry tempMap = (Map.Entry) iterator.next();
            if(tempMap.getKey().equals("checkBoxBerlin")) {checkBoxBerlin.setChecked(true); }
            if(tempMap.getKey().equals("checkBoxBrandenburg")) {checkBoxBrandenburg.setChecked(true); }
            if(tempMap.getKey().equals("checkBoxBarbecueArea")) {checkBoxBarbecueArea.setChecked(true); }
            if(tempMap.getKey().equals("checkBoxGastronomy")) {checkBoxGastronomy.setChecked(true); }
            if(tempMap.getKey().equals("checkBoxLavatory")) {checkBoxLavatory.setChecked(true); }
            if(tempMap.getKey().equals("checkBoxAquatic")) {
                checkBoxAquatic.setChecked(true); }
            if(tempMap.getKey().equals("checkBoxLifeguard")) {checkBoxLifeguard.setChecked(true); }
            if(tempMap.getKey().equals("checkBoxParkingArea")) {checkBoxParkingArea.setChecked(true); }
            if(tempMap.getKey().equals("checkBoxplayground")) {checkBoxPlayground.setChecked(true); }
        }
    }

    @Override
    public void finish() {
        super.finish();
        Log.i(TAG, "finish");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");
    }
}