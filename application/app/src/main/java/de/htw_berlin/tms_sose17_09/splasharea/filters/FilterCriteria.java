package de.htw_berlin.tms_sose17_09.splasharea.filters;

import java.util.List;

import de.htw_berlin.tms_sose17_09.splasharea.models.SplashArea;

/**
 * @version 0.17.0507
 * @author Team SplashArea
 *
 * Dieses Interface ist Bestandteil des Design-Patterns Filter / FilterCriteria
 */
public interface FilterCriteria {
    List<SplashArea> collectCriteria(List<SplashArea> splashAreaList);
}
