package de.htw_berlin.tms_sose17_09.splasharea.asyncTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import de.htw_berlin.tms_sose17_09.splasharea.R;
import de.htw_berlin.tms_sose17_09.splasharea.models.SplashAreaSingleton;

/**
 * @version 0.17.0507
 * @author Team SplashArea
 *
 * Diese Klasse ueberprueft die Internetverbindung.
 */
public class CheckInternetConnectionTask extends AsyncTask<Void, Void, Boolean> {

    Context applicationContext;

    private final String TAG = this.getClass().getSimpleName();


    public CheckInternetConnectionTask(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        Log.i(TAG, "doInBackground");

        try {
            URL url = new URL(applicationContext.getResources().getString(R.string.check_connection_to));
            HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
            urlc.setConnectTimeout(200);
            urlc.connect();
            if (urlc.getResponseCode() == 200) {
                SplashAreaSingleton.getInstance().setInternetAvailable(true);
                return true;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            SplashAreaSingleton.getInstance().setInternetAvailable(false);
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            SplashAreaSingleton.getInstance().setInternetAvailable(false);
            return false;
        }
        SplashAreaSingleton.getInstance().setInternetAvailable(false);
        return false;
    }
}
