package de.htw_berlin.tms_sose17_09.splasharea.filters;

import java.util.ArrayList;
import java.util.List;

import de.htw_berlin.tms_sose17_09.splasharea.models.SplashArea;

/**
 * @version 0.17.0507
 * @author Team SplashArea
 *
 * Diese Klasse stellt eine der zentralen Klassen für die Filterung der Liste dar.
 * Ihr werden die einzelnen Filteroptionen (Filterklassen) und ihren Listen übergeben, damit eine
 * final gefilterte Liste ausgegeben werden kann.
 */
public class SpecificCriteria implements FilterCriteria {

    private List<FilterCriteria> specificFilterCriteriaList = new ArrayList<>();

    public SpecificCriteria(List<FilterCriteria> paramList) {
        this.specificFilterCriteriaList = paramList;
    }


    @Override
    public List<SplashArea> collectCriteria(List<SplashArea> splashAreaList) {

        if (specificFilterCriteriaList == null) {
            return null;
        }

        List<SplashArea> specificSplashAreaList = new ArrayList<>();

        for (int i = 0; i < specificFilterCriteriaList.size(); i++) {
            if (i == 0) {
                specificSplashAreaList = specificFilterCriteriaList.get(0).collectCriteria(splashAreaList);
            } else {
                specificSplashAreaList = specificFilterCriteriaList.get(i).collectCriteria(specificSplashAreaList);
            }
        }
        return specificSplashAreaList;
    }
}
