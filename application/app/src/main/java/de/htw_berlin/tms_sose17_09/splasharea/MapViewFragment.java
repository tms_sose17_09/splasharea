/**
 * source:
 * https://stackoverflow.com/questions/19353255/how-to-put-google-maps-v2-on-a-fragment-using-viewpager
 */

package de.htw_berlin.tms_sose17_09.splasharea;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ToggleButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.OnMapReadyCallback;

import de.htw_berlin.tms_sose17_09.splasharea.models.SplashArea;
import de.htw_berlin.tms_sose17_09.splasharea.models.SplashAreaSingleton;

import static android.content.Context.LOCATION_SERVICE;

/**
 * @version 0.17.0507
 * @author Team SplashArea
 *
 * Diese Klasse stellt das Fragment dar, welche die (google)Map beinhaltet, welche die Marker anzeigt,
 * sowie die aktuelle GPS-Koordinate.
 */
public class MapViewFragment extends Fragment implements LocationListener, View.OnClickListener {

    private final String TAG = this.getClass().getSimpleName();


    private MapView mMapView;
    private GoogleMap googleMap;

    //private Switch gpsSwitch;
    private ToggleButton gpsSwitch;


    private LocationManager locationManager;
    private LocationListener locationListener;

    Marker addMarker;

    private LatLng gpsCoordinate;
    private static boolean isGpsOn = false;
    // check when gps runs before another activity will start
    private static boolean wasGpsOn = false;

    private String provider;

    private String[] listPermissions = {
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION};

    private  static final int REQUEST_START_LOCATION_UPDATES = 1000;
    private  static final int REQUEST_FOR_CURRENT_LOCATION = 1001;

    public MapViewFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.layout_mapview_fragment, container, false);
        Log.i(TAG, "onCreateView");

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        gpsSwitch = (ToggleButton) rootView.findViewById(R.id.gps_switch);
        gpsSwitch.setText(getResources().getString(R.string.gps_button_on));

        gpsSwitch.setOnClickListener(this);
        locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);

        // needed to get the map to display immediately
        mMapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {

            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                if (SplashAreaSingleton.getInstance().isParsed()) {
                    for (SplashArea splashArea : SplashAreaSingleton.getInstance().getFilteredSplashAreaList()) {
                        mMap.addMarker(new MarkerOptions().position(splashArea.getLatLng()).title(splashArea.getBadname()).snippet(GeoFormatter.geoDataformatting(splashArea.getLongitude(), splashArea.getLatitude()))).setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.marker_app_icon));
                    }
                }
                // For zooming automatically to the location of berlin
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(52.516405, 13.398653)).zoom(10).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });

        return rootView;
    }


    void checkPermissionForLocationUpdates(int requestCode) {
        Log.d(TAG,"checkPermissionForLocationUpdates");
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                enableButtons(requestCode);
            } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)) {
                enableButtons(requestCode);
            } else {
                ActivityCompat.requestPermissions(getActivity(), listPermissions, requestCode);
            }
        } else {
            startingLocationServices(requestCode);
        }
    }

    public void enableButtons(int requestCode) {
        Log.d(TAG, "enableButtons");
        if (requestCode == REQUEST_START_LOCATION_UPDATES) {
            gpsSwitch.setEnabled(true);
        }
    }

    public void startingLocationServices(int requestCode) {
        Log.d(TAG, "startingLocationServices");
        if (requestCode == REQUEST_START_LOCATION_UPDATES) {
            try {
                provider = locationManager.getBestProvider(new Criteria(), false);
                if (provider != null) {
                    locationManager.requestLocationUpdates(provider, 4000, 10, this);
                    isGpsOn = true;
                    wasGpsOn = true;
                } else {
                    Log.d(TAG, "kein Provider");
                }
            } catch (SecurityException e) {
                Log.e(TAG, e.getMessage());
            }

            if (isGpsOn) {
                gpsSwitch.setText(getResources().getString(R.string.gps_button_off));
            } else {
                gpsSwitch.setText(getResources().getString(R.string.gps_button_on));
            }
            gpsSwitch.setEnabled(true);
        } else if (requestCode == REQUEST_FOR_CURRENT_LOCATION) {
            try {
                provider = locationManager.getBestProvider(new Criteria(), false);
                if (provider != null) {
                    showLocation(locationManager.getLastKnownLocation(provider));
                } else {
                    Log.d(TAG, "kein Provider");
                }
            } catch (SecurityException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    //LocationListener:
    void showLocation(Location location) {
        Log.d(TAG, "showLocation");
        if (location != null) {
            if(addMarker != null)
                addMarker.remove();
            gpsCoordinate = new LatLng(location.getLatitude(), location.getLongitude());
            addMarker = googleMap.addMarker(new MarkerOptions()
            .position(gpsCoordinate));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionResult");

        boolean onePermissionAllowed = false;

        for (int i = 0; i < grantResults.length; i++) {
            onePermissionAllowed = onePermissionAllowed || (grantResults[i] == PackageManager.PERMISSION_GRANTED);
            Log.d(TAG, "permissions[" + i + "]: " + permissions[i] + " = " + (grantResults[i] == PackageManager.PERMISSION_GRANTED));
        }
        Log.d(TAG, "allowed one");
        if (onePermissionAllowed) {
            startingLocationServices(requestCode);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged");
        showLocation(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d(TAG, "onStatusChanged");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d(TAG, "onProviderEnabled");
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d(TAG, "onProviderDisabled");
    }

    @Override
    public void onClick(View view) {
        Log.d(TAG, "onClick");

        switch (view.getId()) {
            case R.id.gps_switch:
                gpsSwitch.setEnabled(false);
                if (isGpsOn) {
                    try {
                        locationManager.removeUpdates(this);
                    } catch (SecurityException e) {
                        Log.e(TAG, e.getMessage());
                    } finally {
                        gpsSwitch.setText(getResources().getString(R.string.gps_button_on));
                        isGpsOn = false;
                        wasGpsOn = false;
                    }
                    gpsSwitch.setEnabled(true);
                } else {
                    checkPermissionForLocationUpdates(REQUEST_START_LOCATION_UPDATES);
                }
        }
    }


    /*
    ===============================================================================

    basic mathods

    ===============================================================================
     */

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");

        mMapView.onResume();

        mMapView.getMapAsync(new OnMapReadyCallback() {

            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                mMap.clear();
                for (SplashArea splashArea : SplashAreaSingleton.getInstance().getFilteredSplashAreaList()) {
                    mMap.addMarker(new MarkerOptions().position(splashArea.getLatLng()).title(splashArea.getBadname()).snippet(GeoFormatter.geoDataformatting(splashArea.getLongitude(), splashArea.getLatitude()))).setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.marker_app_icon));
                }
                // For zooming automatically to the location of berlin
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(52.516405, 13.398653)).zoom(10).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });

        if(wasGpsOn == true) {
            gpsSwitch.callOnClick();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");

        mMapView.onPause();

        try {
            locationManager.removeUpdates(this);
        } catch (SecurityException e) {
            Log.e(TAG, e.getMessage());
        } finally {
            gpsSwitch.setText(getResources().getString(R.string.gps_button_on));
            isGpsOn = false;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");

        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.i(TAG, "onLowMemory");
        mMapView.onLowMemory();
    }
}