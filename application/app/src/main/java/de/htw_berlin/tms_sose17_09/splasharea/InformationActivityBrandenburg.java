package de.htw_berlin.tms_sose17_09.splasharea;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import de.htw_berlin.tms_sose17_09.splasharea.models.SplashAreaSingleton;

/**
 * @version 0.17.0507
 * @author Team SplashArea
 *
 * Die Klasse haelt die Informationen, welche fuer den Informationsscreen benötigt werde.
 */
public class InformationActivityBrandenburg extends AppCompatActivity {

    private final String TAG = this.getClass().getSimpleName();

    private final String TRUE = "vorhanden";
    private final String FALSE = "nicht vorhanden";

    private TextView textViewBadname;
    private TextView textViewAdresse;
    private TextView textViewProfil;
    private TextView textViewTemperatur;
    private TextView textViewVisibilityDept;
    private TextView textViewBeachCharacter;
    private TextView textViewLicensee;
    private TextView textViewLicenseeUrl;
    private TextView textViewLavatory;
    private TextView textViewWasteDisposal;
    private TextView textViewGastronomy;
    private TextView textViewLifeguard;
    private TextView textViewParkingArea;
    private TextView textViewSunbathingArea;
    private TextView textViewFishing;
    private TextView textViewCamping;
    private TextView textViewPlayground;
    private TextView textViewBarbecueArea;
    private TextView textViewAquatics;
    private TextView textViewMiscellaneos;
    private TextView textViewDetailDescription;
    private TextView textViewStand;
    private TextView textViewKoordinaten;

    private ImageView imageViewLavatoryTrue;
    private ImageView imageViewLavatoryFalse;
    private ImageView imageViewWasteDisposalTrue;
    private ImageView imageViewWasteDisposalFalse;
    private ImageView imageViewGastronomyTrue;
    private ImageView imageViewGastronomyFalse;
    private ImageView imageViewLifeguardTrue;
    private ImageView imageViewLifeguardFalse;
    private ImageView imageViewParkingAreaTrue;
    private ImageView imageViewParkingAreaFalse;
    private ImageView imageViewSunbathingAreaTrue;
    private ImageView imageViewSunBathingAreaFalse;
    private ImageView imageViewFishingTrue;
    private ImageView imageViewFishingFalse;
    private ImageView imageViewCampingTrue;
    private ImageView imageViewCampingFalse;
    private ImageView imageViewPlaygroundTrue;
    private ImageView ImageViewPlaygroundFalse;
    private ImageView imageViewBarbecueAreaTrue;
    private ImageView imageViewBarbecueAreaFalse;
    private ImageView imageViewAquaticsTrue;
    private ImageView imageViewAquaticsFalse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");

        setContentView(R.layout.layout_information_brandenburg_activity);

        initalUISetup();

        // get intent extra
        // the id-value from the item which is sending from the MainActivity-ListFragment->Entry
        final Bundle extras = getIntent().getExtras();

        // get singleton for unsing the list
        SplashAreaSingleton splashAreaSingleton = SplashAreaSingleton.getInstance();

        // searching id into the SingletonList
        int id = Integer.parseInt(extras.getString("id"));

        textViewBadname.setText(splashAreaSingleton.getSplashAreaList().get(id).getBadname());
        textViewAdresse.setText(splashAreaSingleton.getSplashAreaList().get(id).getAdresse());
        textViewProfil.setText(splashAreaSingleton.getSplashAreaList().get(id).getProfil());
        textViewKoordinaten.setText(GeoFormatter.geoDataformatting(splashAreaSingleton.getSplashAreaList().get(id).getLongitude(), splashAreaSingleton.getFilteredSplashAreaList().get(id).getLatitude()));
        textViewTemperatur.setText(splashAreaSingleton.getSplashAreaList().get(id).getTemp());
        textViewVisibilityDept.setText(splashAreaSingleton.getSplashAreaList().get(id).getVisibilityDept());
        textViewBeachCharacter.setText(splashAreaSingleton.getSplashAreaList().get(id).getBeachCharacter());
        textViewLicensee.setText(lineBreak(splashAreaSingleton.getSplashAreaList().get(id).getLicensee()));
        textViewLicenseeUrl.setText(lineBreak(splashAreaSingleton.getSplashAreaList().get(id).getLicenseeUrl()));

        textViewLavatory.setText(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isLavatory()));
        if(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isLavatory()).equals(TRUE)) {
            imageViewLavatoryFalse.setVisibility(View.INVISIBLE);
        } else {
            imageViewLavatoryTrue.setVisibility(View.INVISIBLE);
        }

        textViewWasteDisposal.setText(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isWasteDisposal()));
        if(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isWasteDisposal()).equals(TRUE)) {
            imageViewWasteDisposalFalse.setVisibility(View.INVISIBLE);
        } else {
            imageViewWasteDisposalTrue.setVisibility(View.INVISIBLE);
        }

        textViewGastronomy.setText(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isGastronomy()));
        if(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isGastronomy()).equals(TRUE)) {
            imageViewGastronomyFalse.setVisibility(View.INVISIBLE);
        } else {
            imageViewGastronomyTrue.setVisibility(View.INVISIBLE);
        }

        textViewLifeguard.setText(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isLifeguard()));
        if(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isLifeguard()).equals(TRUE)) {
            imageViewLifeguardFalse.setVisibility(View.INVISIBLE);
        } else {
            imageViewLifeguardTrue.setVisibility(View.INVISIBLE);
        }
        textViewParkingArea.setText(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isParkingArea()));
        if(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isParkingArea()).equals(TRUE)) {
            imageViewParkingAreaFalse.setVisibility(View.INVISIBLE);
        } else {
            imageViewParkingAreaTrue.setVisibility(View.INVISIBLE);
        }

        textViewSunbathingArea.setText(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isSunbathingArea()));
        if(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isSunbathingArea()).equals(TRUE)) {
            imageViewSunBathingAreaFalse.setVisibility(View.INVISIBLE);
        } else {
            imageViewSunbathingAreaTrue.setVisibility(View.INVISIBLE);
        }

        textViewFishing.setText(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isFishing()));
        if(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isFishing()).equals(TRUE)) {
            imageViewFishingFalse.setVisibility(View.INVISIBLE);
        } else {
            imageViewFishingTrue.setVisibility(View.INVISIBLE);
        }

        textViewCamping.setText(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isCamping()));
        if(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isCamping()).equals(TRUE)) {
            imageViewCampingFalse.setVisibility(View.INVISIBLE);
        } else {
            imageViewCampingTrue.setVisibility(View.INVISIBLE);
        }

        textViewPlayground.setText(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isPlayground()));
        if(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isPlayground()).equals(TRUE)) {
            ImageViewPlaygroundFalse.setVisibility(View.INVISIBLE);
        } else {
            imageViewPlaygroundTrue.setVisibility(View.INVISIBLE);
        }

        textViewBarbecueArea.setText(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isBarbecueArea()));
        if(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isBarbecueArea()).equals(TRUE)) {
            imageViewBarbecueAreaFalse.setVisibility(View.INVISIBLE);
        } else {
            imageViewBarbecueAreaTrue.setVisibility(View.INVISIBLE);
        }

        textViewAquatics.setText(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isAquatics()));
        if(trueFalseTranlator(splashAreaSingleton.getSplashAreaList().get(id).isAquatics()).equals(TRUE)) {
            imageViewAquaticsFalse.setVisibility(View.INVISIBLE);
        } else {
            imageViewAquaticsTrue.setVisibility(View.INVISIBLE);
        }

        textViewMiscellaneos.setText(splashAreaSingleton.getSplashAreaList().get(id).getMiscellaneous());
        textViewDetailDescription.setText(splashAreaSingleton.getSplashAreaList().get(id).getDetailDescription());

        textViewStand.setText(splashAreaSingleton.getSplashAreaList().get(id).getStand());

    }

    private void initalUISetup() {

        textViewBadname = (TextView) findViewById(R.id.textView_info_badname);
        textViewAdresse = (TextView) findViewById(R.id.textView_info_address);
        textViewProfil = (TextView) findViewById(R.id.textView_info_profil);
        textViewKoordinaten = (TextView) findViewById(R.id.textView_info_coordinate);
        textViewTemperatur = (TextView) findViewById(R.id.textView_info_temp);
        textViewStand = (TextView) findViewById(R.id.textView_info_stand);
        textViewVisibilityDept = (TextView) findViewById(R.id.textView_info_visibilityDept);
        textViewBeachCharacter = (TextView) findViewById(R.id.textView_info_beachCharacter);
        textViewLicensee = (TextView) findViewById(R.id.textView_info_licensee);
        textViewLicenseeUrl = (TextView) findViewById(R.id.textView_info_licenseeUrl);
        textViewLavatory = (TextView) findViewById(R.id.textView_info_lavatory);
        textViewWasteDisposal = (TextView) findViewById(R.id.textView_info_wasteDisposal);
        textViewGastronomy = (TextView) findViewById(R.id.textView_info_gastronomy);
        textViewLifeguard = (TextView) findViewById(R.id.textView_info_lifeguard);
        textViewParkingArea = (TextView) findViewById(R.id.textView_info_parkingArea);
        textViewSunbathingArea = (TextView) findViewById(R.id.textView_info_sunbatingArea);
        textViewFishing = (TextView) findViewById(R.id.textView_info_fishing);
        textViewCamping = (TextView) findViewById(R.id.textView_info_camping);
        textViewPlayground = (TextView) findViewById(R.id.textView_info_playgroung);
        textViewBarbecueArea = (TextView) findViewById(R.id.textView_info_barbecueArea);
        textViewAquatics = (TextView) findViewById(R.id.textView_info_aquatics);
        textViewMiscellaneos = (TextView) findViewById(R.id.textView_info_miscellaneous);
        textViewDetailDescription = (TextView) findViewById(R.id.textView_info_detailDescription);

        imageViewLavatoryTrue = (ImageView) findViewById(R.id.imageView_lavatory_true);
        imageViewLavatoryFalse = (ImageView) findViewById(R.id.imageView_lavatory_false);
        imageViewWasteDisposalTrue = (ImageView) findViewById(R.id.imageView_wasteDisposal_true);
        imageViewWasteDisposalFalse = (ImageView) findViewById(R.id.imageView_wasteDisposal_false);
        imageViewGastronomyTrue = (ImageView) findViewById(R.id.imageView_gastronomy_true);
        imageViewGastronomyFalse = (ImageView) findViewById(R.id.imageView_gastronomy_false);
        imageViewLifeguardTrue = (ImageView) findViewById(R.id.imageView_lifeguard_true);
        imageViewLifeguardFalse = (ImageView) findViewById(R.id.imageView_lifeguard_false);
        imageViewParkingAreaTrue = (ImageView) findViewById(R.id.imageView_parkingArea_true);
        imageViewParkingAreaFalse = (ImageView) findViewById(R.id.imageView_parkingArea_false);
        imageViewSunbathingAreaTrue = (ImageView) findViewById(R.id.imageView_sunbatingArea_true);
        imageViewSunBathingAreaFalse = (ImageView) findViewById(R.id.imageView_sunbatingArea_false);
        imageViewFishingTrue = (ImageView) findViewById(R.id.imageView_fishing_true);
        imageViewFishingFalse = (ImageView) findViewById(R.id.imageView_fishing_false);
        imageViewCampingTrue = (ImageView) findViewById(R.id.imageView_camping_true);
        imageViewCampingFalse = (ImageView) findViewById(R.id.imageView_camping_false);
        imageViewPlaygroundTrue = (ImageView) findViewById(R.id.imageView_playgroung_true);
        ImageViewPlaygroundFalse = (ImageView) findViewById(R.id.imageView_playgroung_false);
        imageViewBarbecueAreaTrue = (ImageView) findViewById(R.id.imageView_barbecueArea_true);
        imageViewBarbecueAreaFalse = (ImageView) findViewById(R.id.imageView_barbecueArea_false);
        imageViewAquaticsTrue = (ImageView) findViewById(R.id.imageView_aquatics_true);
        imageViewAquaticsFalse = (ImageView) findViewById(R.id.imageView_aquatics_false);
    }

    private String trueFalseTranlator(boolean value) {
        Log.d(TAG, "trueFalseTranlator");

        if (String.valueOf(value).equals("true")) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private String lineBreak(String line) {
        Log.d(TAG, "lineBreak");

        boolean isUrl = true;

        for (int i = 0; i < line.length(); i++) {
            if (Character.isWhitespace(line.charAt(i))) {
                isUrl = false;
            }
        }

        if (isUrl) {
            return line;
        } else {
            String tempString = "";
            String stringParts[] = line.split(" ");
            for (int i = 0; i < stringParts.length; i++) {

                tempString += stringParts[i] + " ";
                if (i % 2 == 1) {
                    tempString += "\n";
                }
            }
            return tempString;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
    }

    @Override
    public void finish() {
        super.finish();
        Log.i(TAG, "finish");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");
    }
}
