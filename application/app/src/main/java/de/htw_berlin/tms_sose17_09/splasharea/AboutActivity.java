package de.htw_berlin.tms_sose17_09.splasharea;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.widget.TextView;

/**
 * @version 0.17.0507
 * @author Team SplashArea
 *
 * Diese Klasse stellt zusaetzliche Infomationen, die App betreffend, dar.
 */
public class AboutActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");

        setContentView(R.layout.activity_about);

        initalUISetup();

    }

    public void initalUISetup() {
        Log.i(TAG, "initalUISetup");

        Typeface typefaceTexRegular = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font_tex_regular));

        TextView textViewTitle = (TextView) findViewById(R.id.textView_about_app_title);
        textViewTitle.setTypeface(typefaceTexRegular);

        // Developer
        TextView textViewDeveloperTitle = (TextView) findViewById(R.id.textView_developer);
        textViewDeveloperTitle.setTypeface(typefaceTexRegular);

        TextView textViewDeveloper01 = (TextView) findViewById(R.id.textView_developer_01);
        textViewDeveloper01.setTypeface(typefaceTexRegular);

        TextView textViewDeveloper02 = (TextView) findViewById(R.id.textView_developer_02);
        textViewDeveloper02.setTypeface(typefaceTexRegular);

        TextView textViewDeveloper03 = (TextView) findViewById(R.id.textView_developer_03);
        textViewDeveloper03.setTypeface(typefaceTexRegular);

        TextView textViewDeveloper04 = (TextView) findViewById(R.id.textView_developer_04);
        textViewDeveloper04.setTypeface(typefaceTexRegular);

        // Git Repo
        TextView textViewGitRepoTitle = (TextView) findViewById(R.id.textView_git_repo_title);
        textViewGitRepoTitle.setTypeface(typefaceTexRegular);

        TextView textViewGitRepo = (TextView) findViewById(R.id.textView_git_repo_url);
        textViewGitRepo.setMovementMethod(LinkMovementMethod.getInstance());
        textViewGitRepo.setTypeface(typefaceTexRegular);

        // Open Data
        TextView textViewBrandenburgTitle = (TextView) findViewById(R.id.textView_data_sources);
        textViewBrandenburgTitle.setTypeface(typefaceTexRegular);

        TextView textViewBrandenburgUebersicht = (TextView) findViewById(R.id.textView_data_brandenburg_overview);
        textViewBrandenburgUebersicht.setMovementMethod(LinkMovementMethod.getInstance());
        textViewBrandenburgUebersicht.setTypeface(typefaceTexRegular);

        TextView textViewBerlinUebersicht = (TextView) findViewById(R.id.textView_data_berlin_overview);
        textViewBerlinUebersicht.setMovementMethod(LinkMovementMethod.getInstance());
        textViewBerlinUebersicht.setTypeface(typefaceTexRegular);

        // Version
        TextView textViewVersionTitle = (TextView) findViewById(R.id.textView_version_title);
        textViewVersionTitle.setTypeface(typefaceTexRegular);

        TextView textViewVersionNr = (TextView) findViewById(R.id.textView_version_nr);
        textViewVersionNr.setTypeface(typefaceTexRegular);

        // Last Update
        TextView textViewLastUpdate = (TextView) findViewById(R.id.textView_last_update);
        textViewLastUpdate.setTypeface(typefaceTexRegular);

        TextView textViewLastUpdateDate = (TextView) findViewById(R.id.textView_last_update_date);
        textViewLastUpdateDate.setTypeface(typefaceTexRegular);
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
    }

    @Override
    public void finish() {
        super.finish();
        Log.i(TAG, "finish");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
    }
}
