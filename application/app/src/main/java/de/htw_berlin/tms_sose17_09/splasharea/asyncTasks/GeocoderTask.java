package de.htw_berlin.tms_sose17_09.splasharea.asyncTasks;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import de.htw_berlin.tms_sose17_09.splasharea.models.SplashAreaSingleton;
import de.htw_berlin.tms_sose17_09.splasharea.models.SplashArea;


/**
 * @version 0.17.0507
 * @author Team SplashArea
 *
 * Diese Klasse wandelt die Geokoordinaten im Hintergrund in Adressen um.
 */
public class GeocoderTask extends AsyncTask<String, String, Boolean> {

    Context applicationContext;

    Geocoder geocoder;

    List<Address> addressList;

    private final String TAG = this.getClass().getSimpleName();

    public GeocoderTask(Context applicationContext) {
        this.applicationContext = applicationContext;
        geocoder = new Geocoder(applicationContext);
    }

    @Override
    protected Boolean doInBackground(String... params) {
        Log.i(TAG, "doInBackground");

        if (SplashAreaSingleton.getInstance().isInternetAvailable() == true && SplashAreaSingleton.getInstance().isParsed()) {
            // Geocoder stuff
            /**
             * key for getAddressLine();
             *  0   Street / no
             *  1   PLZ / City
             *  2   Country
             *
             */
            for (SplashArea splashArea : SplashAreaSingleton.getInstance().getSplashAreaList()) {
                try {
                    addressList = geocoder.getFromLocation(splashArea.getLatitude(), splashArea.getLongitude(), 1);
                    Address address = addressList.get(0);
                    splashArea.setAdresse(address.getAddressLine(0) + ",\n" + address.getAddressLine(1));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return true;
        }
        return false;
    }
}
