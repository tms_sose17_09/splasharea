package de.htw_berlin.tms_sose17_09.splasharea;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import de.htw_berlin.tms_sose17_09.splasharea.models.SplashArea;
import de.htw_berlin.tms_sose17_09.splasharea.models.SplashAreaSingleton;

/**
 * @version 0.17.0507
 * @author Team SplashArea
 *
 * Die Klasse stellt das Listenansicht Fragment dar, welche den Parser innerhalb des AsyncTask bereitstellt.
 * Dieser laeft im Hintergrund und greift entweder auf die URL oder die lokale Datei im Verzeichnis /assets zu.
 */
public class ListFragment extends Fragment{

    private final String TAG = this.getClass().getSimpleName();

    private ListView listViewSplashArea;
    // flipp between load and list view
    private ViewFlipper viewFlipper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.layout_list_fragment, container, false);
        Log.i(TAG, "onCreateView");

        listViewSplashArea = (ListView) rootView.findViewById(R.id.list_view_SplashArea);

        // for loadsceen and listview
        // switch between both layouts
        viewFlipper = (ViewFlipper) rootView.findViewById(R.id.view_flipper);

        if (!SplashAreaSingleton.getInstance().isParsed()) {
            if (SplashAreaSingleton.getInstance().isInternetAvailable()) {
                new ParserTask(getActivity().getApplicationContext()).execute(getContext().getResources().getString(R.string.berlin_url), getContext().getResources().getString(R.string.brandenburg_url));
                Log.d(TAG, "NOT PARSED / INTERNET");
            } else {
                new ParserTask(getActivity().getApplicationContext()).execute(loadJSONFromAsset(), getContext().getResources().getString(R.string.kml_file));
                Log.d(TAG, "NOT PARSED / NO INTERNET");
            }

        } else {
            viewFlipper.showNext();
            SplashAreaAdapter splashAreaAdapter = new SplashAreaAdapter(getActivity().getApplicationContext(), R.layout.layout_row, SplashAreaSingleton.getInstance().getFilteredSplashAreaList());
            listViewSplashArea.setAdapter(splashAreaAdapter);
            Log.d(TAG, "IS PARSED");
        }

        // Get Fragment id
        /*
        Fragment fragment = (Fragment) this;
        String name = fragment.getTag();

        Log.i("INFO", "FRAGMENT_TAG ListFragment: " + name);
        */
        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.i(TAG, "onResume");
        if(SplashAreaSingleton.getInstance().isParsed()) {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
        }

        if (SplashAreaSingleton.getInstance().isParsed()) {
            SplashAreaAdapter splashAreaAdapter = new SplashAreaAdapter(getActivity().getApplicationContext(), R.layout.layout_row, SplashAreaSingleton.getInstance().getFilteredSplashAreaList());
            listViewSplashArea.setAdapter(splashAreaAdapter);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
    }

    /**
     * Load local json file (JSONsplashArea.gjson) from directory (/assets)
     * @return input of the json
     */
    private String loadJSONFromAsset() {
        Log.d(TAG, "loadJSONFromAsset");

        String jsonFile = "";

        try {
            String JSON_LOCAL_FILE = "offlineFiles/JSONsplashArea.gjson";
            InputStream inputStream = getActivity().getAssets().open(getContext().getResources().getString(R.string.json_file));
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            jsonFile = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return jsonFile;
    }

    /**
     * AsyncTask enables proper and easy use of the UI thread. This class allows you to perform
     * background operations and publish results on the UI thread without having to manipulate threads
     * and/or handlers.
     */
    public class ParserTask extends AsyncTask<String, String, List<SplashArea> > {

        private final String TAG = this.getClass().getSimpleName();

        // for Geocoder
        final Context context;

        int idCounter = 0;

        public ParserTask(Context context) {
            this.context = context;
        }

            @Override
            protected List<SplashArea> doInBackground(String... params) {
                Log.i(TAG, "doInBackground");


                HttpURLConnection connection = null;

                InputStream inputStream;

                BufferedReader bufferedReader = null;

                try {
                    //JSON Parsing
                    // connection to the JSON-Ressource
                    // get http or file in params[0]
                    if (params[0].charAt(0) == 'h') { // h for http://...

                        URL url = new URL(params[0]);
                        connection = (HttpURLConnection) url.openConnection();
                        connection.connect();
                        inputStream = connection.getInputStream();
                    } else {
                        // convert string to inputstream
                        inputStream = new ByteArrayInputStream(params[0].getBytes(StandardCharsets.UTF_8));
                    }
                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
                    StringBuffer stringBuffer = new StringBuffer();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuffer.append(line);
                    }

                    String finalJsonString = stringBuffer.toString();

                    // the whole json-file(object)
                    JSONObject jsonObject = new JSONObject(finalJsonString);

                    JSONArray jsonArrayFeatures = jsonObject.getJSONArray("features");

                    SplashAreaSingleton splashAreaSingleton = SplashAreaSingleton.getInstance();

                    for(int i = 0; i < jsonArrayFeatures.length(); i++) {
                        JSONObject jsonObjectFinal = jsonArrayFeatures.getJSONObject(i).getJSONObject("properties").getJSONObject("data");
                        SplashArea splashAreaJSON = new SplashArea();

                        splashAreaJSON.setId(idCounter);
                        idCounter++;
                        splashAreaJSON.setBadname(jsonObjectFinal.getString("badname"));
                        splashAreaJSON.setBezirk(jsonObjectFinal.getString("bezirk"));
                        splashAreaJSON.setStand(jsonObjectFinal.getString("dat"));
                        splashAreaJSON.setTemp(jsonObjectFinal.getString("temp"));
                        splashAreaJSON.setLatitude(jsonObjectFinal.getDouble("latitude"));
                        splashAreaJSON.setLongitude(jsonObjectFinal.getDouble("longitude"));
                        splashAreaJSON.setProfil(jsonObjectFinal.getString("profil"));
                        splashAreaJSON.setWasserqualitaet((float) jsonObjectFinal.getDouble("wasserqualitaet"));
                        splashAreaJSON.setLatLng(splashAreaJSON.getLatitude(), splashAreaJSON.getLongitude());
                        splashAreaJSON.setInBerlin(true);

                        splashAreaSingleton.getSplashAreaList().add(splashAreaJSON);

                        // Show berlin splash areas at the beginning and fill the filteredSplashAreaList.
                        splashAreaSingleton.getFilteredSplashAreaList().add(splashAreaJSON);
                    }

                    /**
                     * KML Parsing
                     *
                     * List entries
                     * ==========================================
                     * 0    Coordinates
                     * 1    lastMeasurementDate
                     * 2    temperature
                     * 3    visibilityDepth
                     * 4    remarks (NOT USED)
                     * 5    name_normalized (NOT USED)
                     * 6    district (NOT USED)
                     * 7    smiley (NOT USED)
                     * 8    bodyOfWater (profile in SplashArea)
                     * 9    name
                     * 10   onr (NOT USED)
                     * 11   bacteriology (NOT USED)
                     * 12   lavatory
                     * 13   wasteDisposal
                     * 14   gastronomy
                     * 15   lifeguard
                     * 16   beachCharacter
                     * 17   picture (NOT USED)
                     * 18   licensee
                     * 19   licenseeUrl
                     * 20   parkingArea
                     * 21   sunbathingArea
                     * 22   fishingArea
                     * 23   campingArea
                     * 24   playground
                     * 25   barbecueArea
                     * 26   aquatics
                     * 27   miscellanous
                     * 28   profile (NOT USED)
                     * 29   detailDescription
                     * 30   rating (NOT USED)
                     *
                     */
                    if (params[1].charAt(0) == 'h') {
                        URL url = new URL(params[1]);
                        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                        inputStream = urlConnection.getInputStream();

                    } else {
                        inputStream = getActivity().getAssets().open(getContext().getResources().getString(R.string.kml_file));
                    }

                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc = dBuilder.parse(inputStream);

                    Element element = doc.getDocumentElement();
                    element.normalize();

                    List<Node> nodeList;

                    NodeList nList = doc.getElementsByTagName("Placemark");


                    for (int j = 0; j < nList.getLength(); j++) {

                        Node node = nList.item(j);

                        if (node.getNodeType() == Node.ELEMENT_NODE) {
                            Element element2 = (Element) node;
                            nodeList = getValues("value", "coordinates", element2);
                            SplashArea splashAreaKML = new SplashArea();

                            splashAreaKML.setInBerlin(false);
                            splashAreaKML.setFiltered();
                            splashAreaKML.setId(idCounter);

                            idCounter++;

                            // COORDINATES(0)
                            splashAreaKML.setLatitude(nodeList.get(0).getNodeValue());
                            splashAreaKML.setLongitude(nodeList.get(0).getNodeValue());
                            splashAreaKML.setLatLng(splashAreaKML.getLatitude(), splashAreaKML.getLongitude());
                            // LASTMEASUREMENTDATE(1)
                            splashAreaKML.setStand(nodeList.get(1).getNodeValue());
                            // TEMPERATURE(2)
                            splashAreaKML.setTemp(nodeList.get(2).getNodeValue());
                            // VISIBILITYDEPTH(3)
                            splashAreaKML.setVisibilityDept(nodeList.get(3).getNodeValue());
                            // BODYOFWATER(8)
                            splashAreaKML.setProfil(nodeList.get(8).getNodeValue());
                            // NAME(9)
                            splashAreaKML.setBadname(nodeList.get(9).getNodeValue());
                            // LAVATORY(12)
                            splashAreaKML.setLavatory(nodeList.get(12).getNodeValue());
                            // WASTEDISPOSAL(13)
                            splashAreaKML.setWasteDisposal(nodeList.get(13).getNodeValue());
                            // GASTRONOMY(14)
                            splashAreaKML.setGastronomy(nodeList.get(14).getNodeValue());
                            // LIFEGUARD(15)
                            splashAreaKML.setLifeguard(nodeList.get(15).getNodeValue());
                            // BEACHCHARACTER(16)
                            splashAreaKML.setBeachCharacter(nodeList.get(16).getNodeValue());
                            // LICENSEE(18)
                            splashAreaKML.setLicensee(nodeList.get(18).getNodeValue());
                            // LICENSEEURL(19)
                            splashAreaKML.setLicenseeUrl(nodeList.get(19).getNodeValue());
                            // PARKINGAREA(20)
                            splashAreaKML.setParkingArea(nodeList.get(20).getNodeValue());
                            // SUNBATHINGAREA(21)
                            splashAreaKML.setSunbathingArea(nodeList.get(21).getNodeValue());
                            // FISHINGAREA(22)
                            splashAreaKML.setFishing(nodeList.get(22).getNodeValue());
                            // CAMPINGAREA(23)
                            splashAreaKML.setCamping(nodeList.get(23).getNodeValue());
                            // PLAYGROUND(24)
                            splashAreaKML.setPlayground(nodeList.get(24).getNodeValue());
                            // BARBECUEAREA(25)
                            splashAreaKML.setBarbecueArea(nodeList.get(25).getNodeValue());
                            // AQUATICS(26)
                            splashAreaKML.setAquatics(nodeList.get(26).getNodeValue());
                            // MISCELLANOUS(27)
                            splashAreaKML.setMiscellaneous(nodeList.get(27).getNodeValue());

                            splashAreaSingleton.getSplashAreaList().add(splashAreaKML);
                        }
                    }

                    SplashAreaSingleton.getInstance().setParsed(true);

                    return splashAreaSingleton.getSplashAreaList();

                } catch (IOException | JSONException | SAXException e1) {
                    e1.printStackTrace();
                } catch (ParserConfigurationException e1) {
                    e1.printStackTrace();
                } finally
                 {
                    if (connection != null) {
                        connection.disconnect();
                    }
                    try {
                        if (bufferedReader != null) {
                            bufferedReader.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }

        private List<Node> getValues(String value, String coordinates, Element element) {
            Log.d(TAG, "getValues");

            NodeList valueNodeList = element.getElementsByTagName(value);
            NodeList coordinatesNodeList = element.getElementsByTagName(coordinates);

            NodeList valueList;
            NodeList coordinatesList;
            List<Node> returnList = new ArrayList<>();

            // add coordinates
            for (int j = 0; j < coordinatesNodeList.getLength(); j++) {
                coordinatesList = element.getElementsByTagName(coordinates).item(j).getChildNodes();
                returnList.add(coordinatesList.item(0));
            }

            // add values
            for (int i = 0; i < valueNodeList.getLength(); i++) {
                valueList = element.getElementsByTagName(value).item(i).getChildNodes();
                returnList.add(valueList.item(0));
            }
            return returnList;
        }

        @Override
        protected void onPostExecute(List<SplashArea> splashAreaList) {
            Log.i(TAG, "onPostExecute");

            super.onPostExecute(SplashAreaSingleton.getInstance().getFilteredSplashAreaList());

            SplashAreaAdapter splashAreaAdapter = new SplashAreaAdapter(getActivity().getApplicationContext(), R.layout.layout_row, SplashAreaSingleton.getInstance().getFilteredSplashAreaList());
            listViewSplashArea.setAdapter(splashAreaAdapter);

            // show listView after parsing
            viewFlipper.showNext();


            // GeocoderTask
            /*
            if(SplashAreaSingleton.getInstance().isInternetAvailable())
                new GeocoderTask(getActivity().getApplicationContext()).execute("");
            */

            // Fragment ID
            MapViewFragment mapViewFragment = (MapViewFragment) getActivity().getSupportFragmentManager().findFragmentByTag("android:switcher:2131558607:1");

            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
        }
    }

    /**
     * Arrayadapter for the ListView
     */
    public class SplashAreaAdapter extends ArrayAdapter {
        private final String TAG = this.getClass().getSimpleName();

        public final List<SplashArea> splashAreaList;
        private final int resource;
        private final LayoutInflater inflater;

        public SplashAreaAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<SplashArea> splashAreaList) {
            super(context, resource, splashAreaList);
            this.splashAreaList = splashAreaList;
            this.resource = resource;
            this.inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            Typeface typefaceTexRegular = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.font_tex_regular));

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.layout_row, parent, false);
            }

            TextView textViewBadname;
            TextView textViewAdresse;
            TextView textViewStandort;
            TextView textViewIdForInfo;

            textViewBadname = (TextView) convertView.findViewById(R.id.textView_badname);
            textViewBadname.setTypeface(typefaceTexRegular);
            textViewAdresse = (TextView) convertView.findViewById(R.id.textView_adresse);
            textViewStandort = (TextView) convertView.findViewById(R.id.textView_LatLng);
            textViewIdForInfo = (TextView) convertView.findViewById(R.id.id_for_infoView);

            textViewBadname.setText(splashAreaList.get(position).getBadname());
            textViewAdresse.setText(splashAreaList.get(position).getAdresse());
            textViewIdForInfo.setText(String.valueOf((int) splashAreaList.get(position).getId()));
            textViewStandort.setText(GeoFormatter.geoDataformatting(splashAreaList.get(position).getLongitude(), splashAreaList.get(position).getLatitude()));

            return convertView;
        }
    }
}