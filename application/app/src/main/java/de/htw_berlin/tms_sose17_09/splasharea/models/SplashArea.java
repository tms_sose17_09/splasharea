package de.htw_berlin.tms_sose17_09.splasharea.models;

import com.google.android.gms.maps.model.LatLng;

/**
 * @version 0.17.0206
 * @author Team SplashArea
 *
 * Diese Klasse stellt die zentrale Datenstruktur für das Projekt dar. Sie wird innerhalb der ArrayList verwendet, welche
 * Teil der Singletonklasse SplashAreaSingleton ist.
 * Der Parser der json-Datei befüllt die jeweiligen Attribute entweder über die angegebene URL oder direkt aus der lokalen JSON-Datei.
 */
public class SplashArea {

    /**
     * Attributes
     */

    //
    // for berlin and brandenburg
    //
    private float id;
    private boolean inBerlin = false;
    private boolean isFiltered = true;
    private String badname;
    private String stand;   // last update
    private String temp;
    private double latitude;
    private double longitude;
    private LatLng latLng;
    private String adresse;
    private String profil;  // Gewaesser BB = bodyOfWater

    //
    // for berlin
    //
    private String bezirk;
    private float wasserqualitaet;

    //
    // for brandenburg
    //
    private String visibilityDept;
    private String beachCharacter; // Sand / Kies
    private String licensee; // Betreiber
    private String licenseeUrl;
    private String miscellaneous;
    private String detailDescription;
    private boolean lavatory; // wc
    private boolean wasteDisposal; // Abfallentsorgung
    private boolean gastronomy;
    private boolean lifeguard;
    private boolean parkingArea;
    private boolean sunbathingArea;
    private boolean fishing;
    private boolean camping;
    private boolean playground;
    private boolean barbecueArea;
    private boolean aquatics;


    /**
     * Getter & Setter
     */

    public boolean isLavatory() {
        return lavatory;
    }

    public void setLavatory(String lavatory) {
        this.lavatory = lavatory.equals("ja");
    }

    public void setLavatory(boolean lavatory) {
        this.lavatory = lavatory;
    }

    public boolean isWasteDisposal() {
        return wasteDisposal;
    }

    public void setWasteDisposal(boolean wasteDisposal) {
        this.wasteDisposal = wasteDisposal;
    }

    public void setWasteDisposal(String wasteDisposal) {
        this.wasteDisposal = wasteDisposal.equals("ja");
    }

    public boolean isGastronomy() {
        return gastronomy;
    }

    public void setGastronomy(boolean gastronomy) {
        this.gastronomy = gastronomy;
    }

    public void setGastronomy(String gastronomy) {
        this.gastronomy = gastronomy.equals("ja");
    }

    public boolean isLifeguard() {
        return lifeguard;
    }

    public void setLifeguard(boolean lifeguard) {
        this.lifeguard = lifeguard;
    }

    public void setLifeguard(String lifeguard) {
        this.lifeguard = lifeguard.equals("ja");
    }

    public boolean isParkingArea() {
        return parkingArea;
    }

    public void setParkingArea(boolean parkingArea) {
        this.parkingArea = parkingArea;
    }

    public void setParkingArea(String parkingArea) {
        this.parkingArea = parkingArea.equals("ja");
    }

    public boolean isSunbathingArea() {
        return sunbathingArea;
    }

    public void setSunbathingArea(boolean sunbathingArea) {
        this.sunbathingArea = sunbathingArea;
    }

    public void setSunbathingArea(String sunbathingArea) {
        this.sunbathingArea = sunbathingArea.equals("ja");
    }

    public boolean isFishing() {
        return fishing;
    }

    public void setFishing(boolean fishing) {
        this.fishing = fishing;
    }

    public void setFishing(String fishing) {
        this.fishing = fishing.equals("ja");
    }

    public boolean isCamping() {
        return camping;
    }

    public void setCamping(boolean camping) {
        this.camping = camping;
    }

    public void setCamping(String camping) {
        this.camping = camping.equals("ja");
    }

    public boolean isPlayground() {
        return playground;
    }

    public void setPlayground(boolean playground) {
        this.playground = playground;
    }

    public void setPlayground(String playground) {
        this.playground = playground.equals("ja");
    }

    public boolean isBarbecueArea() {
        return barbecueArea;
    }

    public void setBarbecueArea(boolean barbecueArea) {
        this.barbecueArea = barbecueArea;
    }

    public void setBarbecueArea(String barbecueArea) {
        this.barbecueArea = barbecueArea.equals("ja");
    }

    public boolean isAquatics() {
        return aquatics;
    }

    public void setAquatics(boolean aquatics) {
        this.aquatics = aquatics;
    }

    public void setAquatics(String aquatics) {
        this.aquatics = aquatics.equals("ja");
    }

    public String getVisibilityDept() {
        return visibilityDept;
    }

    public void setVisibilityDept(String visibilityDept) {
        this.visibilityDept = visibilityDept;
    }

    public String getBeachCharacter() {
        return beachCharacter;
    }

    public void setBeachCharacter(String beachCharacter) {
        this.beachCharacter = beachCharacter;
    }

    public String getLicensee() {
        return licensee;
    }

    public void setLicensee(String licensee) {
        this.licensee = licensee;
    }

    public String getLicenseeUrl() {
        return licenseeUrl;
    }

    public void setLicenseeUrl(String licenseeUrl) {
        this.licenseeUrl = licenseeUrl;
    }

    public String getMiscellaneous() {
        return miscellaneous;
    }

    public void setMiscellaneous(String miscellaneous) {
        this.miscellaneous = miscellaneous;
    }

    public String getDetailDescription() {
        return detailDescription;
    }

    public void setDetailDescription(String detailDescription) {
        this.detailDescription = detailDescription;
    }

    public boolean isInBerlin() {
        return inBerlin;
    }

    public void setInBerlin(boolean inBerlin) {
        this.inBerlin = inBerlin;
    }

    public float getId() {
        return id;
    }

    public void setId(float id) {
        this.id = id;
    }

    public String getBadname() {
        return badname;
    }

    public void setBadname(String badname) {
        this.badname = badname;
    }

    public String getBezirk() {
        return bezirk;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setBezirk(String bezirk) {
        this.bezirk = bezirk;
    }

    public String getStand() {
        return stand;
    }

    public void setStand(String stand) {
        this.stand = "letztes Update: " + stand;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLatitude(String coordinates) {
        String[] parts = coordinates.split(",");
        this.latitude = Double.valueOf(parts[1]);
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLongitude(String coordinates) {
        String[] parts = coordinates.split(",");
        this.longitude = Double.valueOf(parts[0]);
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(double longitude, double latitude) {
        this.latLng = new LatLng(longitude, latitude);
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public void setLatLng(String coordinates) {
        String[] parts = coordinates.split(",");
        this.latLng = new LatLng(Double.valueOf(parts[1]), Double.valueOf(parts[0]));
    }

    public boolean isFiltered() {
        return isFiltered;
    }

    public void setFiltered() {
        isFiltered = false;
    }

    public String getProfil() {
        return profil;
    }

    public void setProfil(String profil) {
        this.profil = profil;
    }

    public float getWasserqualitaet() {
        return wasserqualitaet;
    }

    public void setWasserqualitaet(float wasserqualitaet) {
        this.wasserqualitaet = wasserqualitaet;
    }
}
