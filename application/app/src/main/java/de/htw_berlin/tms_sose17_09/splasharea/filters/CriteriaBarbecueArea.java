package de.htw_berlin.tms_sose17_09.splasharea.filters;

import java.util.ArrayList;
import java.util.List;

import de.htw_berlin.tms_sose17_09.splasharea.models.SplashArea;

/**
 * @version 0.17.0507
 * @author Team SplashArea
 *
 * Diese Klasse ist eine Filteroptioin, die auf die zu filternede Liste angewendet wird.
 */
public class CriteriaBarbecueArea implements FilterCriteria {
    @Override
    public List<SplashArea> collectCriteria(List<SplashArea> splashAreaList) {
        List<SplashArea> barbecueAreaSplashAreaList = new ArrayList<>();

        for (SplashArea splashArea: splashAreaList) {
            if(splashArea.isBarbecueArea()) {
                barbecueAreaSplashAreaList.add(splashArea);
            }
        }
        return barbecueAreaSplashAreaList;
    }
}
