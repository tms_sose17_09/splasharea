package de.htw_berlin.tms_sose17_09.splasharea;

import android.content.Intent;
import android.os.Bundle;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import de.htw_berlin.tms_sose17_09.splasharea.asyncTasks.CheckInternetConnectionTask;
import de.htw_berlin.tms_sose17_09.splasharea.asyncTasks.GeocoderTask;
import de.htw_berlin.tms_sose17_09.splasharea.models.SplashAreaSingleton;


/**
 * @version 0.17.0507
 * @author Team SplashArea
 *
 * Diese Klasse stellt die Container-Klasse dar, welche die Fragements "MapViewFragment" und "ListFragment"
 * beinhaltet.
 */
public class MainActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getSimpleName();

    private boolean isActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        setContentView(R.layout.layout_main_activity);

        // used layout_toolbar for title and additional options
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);

        /*
        PagerAdapter that will provide fragments for each of the sections. We use a FragmentPagerAdapter
        derivative, which will keep every loaded fragment in memory. If this becomes too memory intensive, it
        may be best to switch to a FragmentStatePagerAdapter.
        */
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        // The ViewPager that will host the section contents.
        ViewPager viewPager = (ViewPager) findViewById(R.id.container);
        viewPager.setAdapter(sectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu");
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Select item in the layout_toolbar
     * @param item using item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected");

        int res_id = item.getItemId();
        if(res_id == R.id.action_filter) {
            Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
            startActivity(intent);
        }

        if(res_id == R.id.action_info) {
            Intent intent = new Intent(getApplicationContext(), AboutActivity.class);
            startActivity(intent);
        }

        if(res_id == R.id.action_generate_address) {
            new CheckInternetConnectionTask(this).execute();

            for(int i = 0; i <= 10000000; i++){}


            if(SplashAreaSingleton.getInstance().isInternetAvailable()) {
                new GeocoderTask(this).execute("");
            } else {
                Toast.makeText(this,getResources().getString(R.string.toast_msg_no_internet),Toast.LENGTH_LONG).show();
            }

        }

        return true;
    }

    /**
     * A FragmentPagerAdapter that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     *
     * Selectet fragment
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        private final String TAG = this.getClass().getSimpleName();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Log.d(TAG, "getItem");

            // Returning the current tabs
            switch (position) {
                case 0:
                    return new ListFragment();
                case 1:
                    return new MapViewFragment();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            //Log.d(TAG, "getCount");

            // Show  total pages.
            return 2;
        }

        /**
         * Titel into the tab.
         * @param position
         * @return
         */
        @Override
        public CharSequence getPageTitle(int position) {
            Log.d(TAG, "getPageTitle");

            switch (position) {
                case 0:
                    return "List";
                case 1:
                    return "Map";
            }
            return null;
        }
    }

    /**
     * Sending the id of the selectet item in the layout_row of the listview to the informationview.
     * @param row selectet layout_row
     */
    public void goToInfoView (View row) {
        Log.d(TAG, "goToInfoView");

        TextView splashAreaIdView = (TextView) row.findViewById(R.id.id_for_infoView);
        String splashAreaId = splashAreaIdView.getText().toString();

        if((SplashAreaSingleton.getInstance().getSplashAreaList().get(Integer.parseInt(splashAreaId)).isInBerlin())) {
            Intent intent = new Intent(this, InformationActivityBerlin.class);

            intent.putExtra("id", splashAreaId);

            startActivity(intent);
        } else {
            Intent intent = new Intent(this, InformationActivityBrandenburg.class);

            intent.putExtra("id", splashAreaId);

            startActivity(intent);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
    }

    @Override
    public void finish() {
        super.finish();
        Log.i(TAG, "finish");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
    }
}
