package de.htw_berlin.tms_sose17_09.splasharea.models;

import java.util.ArrayList;
import java.util.List;

/**
 * @version 0.17.0206
 * @author Team SplashArea
 */
public class SplashAreaSingleton {

    private static SplashAreaSingleton splashAreaSingleton = null;
    private final List<SplashArea> splashAreaList = new ArrayList<>();
    private List<SplashArea> filteredSplashAreaList = new ArrayList<>();

    private boolean parsed = false;
    private boolean geocoded = false;
    private boolean internetAvailable = false;

    private SplashAreaSingleton() {}

    public static SplashAreaSingleton getInstance() {
        if (splashAreaSingleton == null) {
            splashAreaSingleton = new SplashAreaSingleton();
        }

        return splashAreaSingleton;
    }

    public List<SplashArea> getSplashAreaList() {
        return splashAreaList;
    }

    public void setFilteredSplashAreaList(List<SplashArea> filteredSplashAreaList) {
        this.filteredSplashAreaList = filteredSplashAreaList;
    }

    public List<SplashArea> getFilteredSplashAreaList () {
        return filteredSplashAreaList;
    }

    public boolean isParsed() {
        return parsed;
    }

    public void setParsed(boolean parsed) {
        this.parsed = parsed;
    }

    public boolean isGeocoded() {
        return geocoded;
    }

    public void setGeocoded(boolean geocoded) {
        this.geocoded = geocoded;
    }

    public boolean isInternetAvailable() {
        return internetAvailable;
    }

    public void setInternetAvailable(boolean internetAvailable) {
        this.internetAvailable = internetAvailable;
    }

}
