package de.htw_berlin.tms_sose17_09.splasharea;

/**
 * @version 0.17.0507
 * @author Team SplashArea
 *
 * Die Klasse rechnet die double werte in lesbare Werte um.
 */
public class GeoFormatter {

    private static String geoformatting(double geodat) {
        int ddd = (int) geodat;
        int mm = (int) ((geodat - ddd) * 60);
        int ss = (int) ((((geodat - ddd) * 60) - mm) * 60);
        return ddd + "°" + mm + "'" + ss;
    }

    public static String geoDataformatting(double latitude, double longitude) {

        return "E" + geoformatting(longitude) + " / N" + geoformatting(latitude);
    }
}


