## Sources
* phone mockup

        https://cdn.pixabay.com/photo/2015/06/14/15/12/smartphone-808994_960_720.png

* swim icon

        https://www.erlangen.de/Portaldata/1/Resources/030_Leben_in_er/bilder/quali_3sterne.JPG?maxwidth=850

* Bild Badesee

        https://cdn.dolomitenstadt.at/wp-content/uploads/2012/04/Fruehling_Tristachersee-600x359.jpg

* Bilder (UI Creative Commons Zero (CC0) license)

        http://freeforcommercialuse.net/page/3/?s=water

---
### Farbschemata
Farbe | HexWert | Beschreibung
--- | --- | --- |
hell blau | 00b6e3 | Corporate Desing - einheitliches Farbschemata
weiß | FFFFFF | Schriftfarbe für Überschriften (Menü und Titel)
blau | 358af3 | mikrobiologisch nicht zu beanstanden
gelb | ffd620 | mikrobiologisch zu beanstanden
orange | ff9500 | Empfehlung - nicht baden
rot | a81010 | Badeverbot, zeitweilig
schwarz | 403939 | Badeverbot, dauerhaft
